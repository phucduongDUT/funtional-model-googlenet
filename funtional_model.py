from numpy import concatenate
from tensorflow.python.keras import Input, Model
from tensorflow.python.keras.layers import Conv2D, BatchNormalization, Activation, MaxPooling2D, AveragePooling2D, \
    Dropout, Flatten, Dense


def minigooglenet_functional(width, height, depth, classes):
    def conv_module(x, K, kX, kY, stride, chanDim, padding="same"):
        # define a CONV => BN => RELU pattern
        x = Conv2D(K, (kX, kY), strides=stride, padding=padding)(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = Activation("relu")(x)
        # return the block
        return x


    def inception_module(x,numk1x1,numk2x3,chanDim):
        conv_1x1 = conv_module(x, numk1x1, 1, 1, (1, 1), chanDim)
        conv_3x3 = conv_module(x, numk2x3, 3, 3, (1, 1), chanDim)
        x = concatenate([conv_1x1,conv_3x3],axis=chanDim)
        return x


    def downsample_module(x, K, chanDim):
        conv_3x3 = conv_module(x, K, 3, 3, (2,2),chanDim,padding="valid")
        pool = MaxPooling2D((3,3), strides=(2,2))(x)
        x = concatenate([conv_3x3,pool], axis=chanDim)
        return x


        inputShape = (height, width, depth)
        chanDim = -1

        # define the model input and first CONV module
        inputs = Input(shape=inputShape)
        x = conv_module(input, 96, 3, 3, (1, 1), chanDim)

        # two Inception modules followed by a downsample module
        x = inception_module(x, 32, 32, chanDim)
        x = inception_module(x, 32, 48, chanDim)
        x = downsample_module(x, 80, chanDim)

        # four Inception modules followed by a downsample module
        x = inception_module(x, 122, 48)
        x = inception_module(x, 96, 64, chanDim)
        x = inception_module(x, 80, 80, chanDim)
        x = inception_module(x, 48, 96, chanDim)
        x = downsample_module(x, 96, chanDim)
        # two Inception modules followed by global POOL and dropout
        x = inception_module(x, 176, 160, chanDim)
        x = inception_module(x, 176, 160, chanDim)
        x = AveragePooling2D((7, 7))(x)
        x = Dropout(0.4)(x)
        #flatten
        x = Flatten()(x)
        x = Dense(classes, activation="softmax")(x)

        model = Model(inputs,x, name="minigooglenet")
        return model








